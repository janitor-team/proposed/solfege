Source: solfege
Section: gnome
Priority: optional
Maintainer: Francois Mazen <francois@mzf.fr>
Build-Depends: debhelper,
               debhelper-compat (= 13),
               dh-python,
               docbook-xml,
               docbook-xsl,
               gettext,
               gir1.2-gtk-3.0,
               itstool,
               jdupes,
               lilypond,
               python3-dev,
               python3-gi,
               python3-gi-cairo,
               swig,
               texinfo,
               txt2man,
               xauth,
               xsltproc,
               xvfb
Rules-Requires-Root: no
Standards-Version: 4.5.1
Homepage: https://www.gnu.org/software/solfege/
Vcs-Browser: https://salsa.debian.org/mzf/solfege
Vcs-Git: https://salsa.debian.org/mzf/solfege.git

Package: solfege
Architecture: all
Depends: freepats,
         gir1.2-gtk-3.0,
         python3-apt,
         python3-gi,
         python3-gi-cairo,
         timidity,
         sensible-utils,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Recommends: csound, solfege-doc
Description: Ear training software
 GNU Solfege is an ear training program for X Windows written in Python,
 using the GTK+ 3.0 libraries. You can practice harmonic and
 melodic intervals, chords, scales and rhythms, and you can add new
 exercises using a simple plain text file format.
 .
 Ear training is a big subject with many connections to music theory
 and performance of music.
 .
 To use this software, you need some basic knowledge about music theory.

Package: solfege-doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Section: doc
Description: Ear training software - documentation
 GNU Solfege is an ear training program for X Windows written in Python,
 using the GTK+ 3.0 libraries. You can practice harmonic and
 melodic intervals, chords, scales and rhythms, and you can add new
 exercises using a simple plain text file format.
 .
 This package contains the documentation files for GNU Solfege.

Package: solfege-oss
Architecture: any
Multi-Arch: same
Depends: solfege, ${misc:Depends}, ${shlibs:Depends}
Description: Ear training software - OSS support module
 GNU Solfege is an ear training program for X Windows written in Python,
 using the GTK+ 3.0 libraries. You can practice harmonic and
 melodic intervals, chords, scales and rhythms, and you can add new
 exercises using a simple plain text file format.
 .
 This package contains the Python module GNU Solfege needs to access
 the sequencer device of OSS.
